package com.pernix.budget.ws;

import com.itextpdf.text.DocumentException;
import com.pernix.budget.factories.HTMLFactory;
import com.pernix.budget.factories.PDFFactory;
import com.pernix.budget.pojos.RevisionRequest;
import com.pernix.budget.transformer.CanvasTransformer;
import com.pernix.budget.ws.utils.WSConstants;

import javax.ws.rs.*;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;

@Path(WSConstants.PDF_WS)
public class PdfReport {

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces("application/pdf")
  public Response createPdf(RevisionRequest revisionRequest) {
    revisionRequest.getRevision().setCanvas(CanvasTransformer.transformToBase64Png(revisionRequest));
    String reportFile = (revisionRequest.getLanguage().equals("spanish"))?"report-spanish.template.vm":"report-english.template.vm";
    String stringReport = HTMLFactory.generateHTML(revisionRequest, reportFile);
    Response response = null;
    try {
      ByteArrayOutputStream file = (ByteArrayOutputStream) PDFFactory.createPDF(stringReport);

      CacheControl cc = new CacheControl();
      cc.setMaxAge(0);
      cc.setPrivate(true);

      response = Response
        .ok()
        .type("application/pdf")
        .entity(file.toByteArray())
        .cacheControl(cc)
        .build();
    }catch(IOException | DocumentException e) {
      e.printStackTrace();
    }
    return response;
  }
}
