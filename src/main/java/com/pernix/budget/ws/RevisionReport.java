package com.pernix.budget.ws;

import com.itextpdf.text.DocumentException;
import com.pernix.budget.factories.HTMLFactory;
import com.pernix.budget.factories.PDFFactory;
import com.pernix.budget.mailer.MailService;
import com.pernix.budget.mailer.MailServiceFactory;
import com.pernix.budget.pojos.RevisionRequest;
import com.pernix.budget.pojos.RevisionResponse;
import com.pernix.budget.transformer.CanvasTransformer;
import com.pernix.budget.ws.utils.WSConstants;

import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

@Path(WSConstants.REVISIONS_WS)
public class RevisionReport {
  private static final Logger LOGGER = Logger.getLogger( MailService.class.getName() );

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RevisionResponse createRevision(RevisionRequest revisionRequest) throws DocumentException {
        RevisionResponse response = new RevisionResponse();
        MailService mailService =  MailServiceFactory.getMailService();
        try {
            ArrayList<String> email = new ArrayList<>();
            email.add(revisionRequest.getEmail());

            if(!revisionRequest.getCcMail().isEmpty()){
                email.add(revisionRequest.getCcMail());
            }
            revisionRequest.getRevision().setCanvas(CanvasTransformer.transformToBase64Png(revisionRequest));

            String languageReport = (revisionRequest.getLanguage().equals("spanish"))?"report-spanish.template.vm":"report-english.template.vm";
            String languageRevision = (revisionRequest.getLanguage().equals("spanish"))?"revision-spanish.template.vm":"revision-english.template.vm";
            String emailSubject = (revisionRequest.getLanguage().equals("spanish"))?"Budget Rent a Car - Su Revisión":"Budget Rent a Car - Your Revision";
            String stringReport = HTMLFactory.generateHTML(revisionRequest, languageReport);
            ByteArrayOutputStream[] attachments = {(ByteArrayOutputStream) PDFFactory.createPDF(stringReport)};

          mailService.generateAndSendEmail(
            email,
            emailSubject,
            HTMLFactory.generateHTML(revisionRequest, languageRevision),
            attachments);

            response.setMessage("Revision was sent!");
            response.setStatus(WSConstants.STATUS_OK);
        } catch (IOException e) {
          response.setStatus(WSConstants.STATUS_FAILED);
          response.setMessage(e.getMessage());
        } catch (SendFailedException e) {
          response.setStatus(WSConstants.STATUS_FAILED);
          response.setMessage(e.getMessage());
        } catch (MessagingException me) {
          response.setStatus(WSConstants.STATUS_FAILED);
          response.setMessage(me.getMessage());
        }

        return response;
    }
}
