package com.pernix.budget.mailer;

import java.io.*;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;

public class MailService {

  static Properties mailServerProperties;
  private Session session;
  static Session getMailSession;

  public MailService(Session session, Properties properties) {
      MailService.mailServerProperties = properties;
      MailService.getMailSession = session;
      init();
  }

  public final void init() {
    Properties prop = new Properties();
    prop.put("mail.smtp.auth", true);
    prop.put("mail.smtp.starttls.enable", "true");
    prop.put("mail.smtp.host", "186.177.85.156");
    prop.put("mail.smtp.port", "26");
    prop.put("mail.smtp.ssl.trust", "186.177.85.156");
    session = Session.getInstance(prop, new Authenticator() {
      @Override
      protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(mailServerProperties.getProperty("budget.email.user"), mailServerProperties.getProperty("budget.email.password"));
      }
    });
  }

  public void generateAndSendEmail(ArrayList<String> recipients, String subject, String body, ByteArrayOutputStream[] attachmentsFiles) throws MessagingException {
    Message message = new MimeMessage(session);
    message.setFrom(new InternetAddress(mailServerProperties.getProperty("budget.email.user")));

    for (String recipient : recipients) {
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
    }

    message.setSubject(subject);

    MimeBodyPart mimeBodyPart = new MimeBodyPart();
    mimeBodyPart.setContent(body, "text/html");

    Multipart multipart = new MimeMultipart();
    multipart.addBodyPart(mimeBodyPart);


    for (ByteArrayOutputStream filename : attachmentsFiles) {
      BodyPart messageBodyPart2 = new MimeBodyPart();
      DataSource attachment = new ByteArrayDataSource(filename.toByteArray(), "application/pdf");
      messageBodyPart2.setDataHandler(new DataHandler(attachment));
      messageBodyPart2.setFileName("report.pdf");
      multipart.addBodyPart(messageBodyPart2);
    }

    message.setContent(multipart);

    Transport.send(message);
  }
}
