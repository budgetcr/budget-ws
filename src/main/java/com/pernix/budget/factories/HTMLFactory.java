package com.pernix.budget.factories;

import com.pernix.budget.pojos.RevisionRequest;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.StringWriter;

public class HTMLFactory {

    public static String generateHTML(RevisionRequest revisionRequest, String file){
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty("resource.loader", "class");
        velocityEngine.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

        velocityEngine.init();
        Template template = velocityEngine.getTemplate(file);
        VelocityContext context = new VelocityContext();
        context.put("revisionRequest", revisionRequest);
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }
}
