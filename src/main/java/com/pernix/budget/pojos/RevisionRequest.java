package com.pernix.budget.pojos;

public class RevisionRequest {

    private Damage[] damages;
    private String[] observations;
    private Revision revision;
    private String email;
    private String ccMail;
    private String contractNumber;
    private String vehicleType;
    private String language;
    private boolean signature;
    private String comment;

    public String getCcMail() {
        return ccMail;
    }

    public void setCcMail(String ccMail) {
        this.ccMail = ccMail;
    }

    public String getLanguage() {
      return language;
    }

    public void setLanguage(String language) {
      this.language = language;
    }

    public boolean getSignature(){
      return signature;
    }

    public void setSignature(boolean signature){
      this.signature = signature;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Damage[] getDamages() {
        return damages;
    }

    public void setDamages(Damage[] damages) {
        this.damages = damages;
    }

    public String[] getObservations() {
        return observations;
    }

    public void setObservations(String[] observations) {
        this.observations = observations;
    }

    public Revision getRevision() {
        return revision;
    }

    public void setRevision(Revision revision) {
        this.revision = revision;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
