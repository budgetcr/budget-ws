package com.pernix.budget.pojos;

public class CarParts {

  private boolean antenna;
  private boolean legal_documents;
  private String  emblems;
  private boolean tools;
  private boolean emergency_kit;
  private String  plates;
  private boolean rack;
  private boolean carpet;
  private boolean hood;
  private boolean trunk;
  private boolean hubcaps;

  public boolean getAntenna() {
    return antenna;
  }

  public void setAntenna(boolean antenna) {
    this.antenna = antenna;
  }

  public boolean getLegal_documents() {
    return legal_documents;
  }

  public void setLegal_documents(boolean legal_documents) {
    this.legal_documents = legal_documents;
  }

  public String getEmblems() {
    return emblems;
  }

  public void setEmblems(String emblems) {
    this.emblems = emblems;
  }

  public boolean getTools() {
    return tools;
  }

  public void setTools(boolean tools) {
    this.tools = tools;
  }

  public boolean getEmergency_kit() {
    return emergency_kit;
  }

  public void setEmergency_kit(boolean emergency_kit) {
    this.emergency_kit = emergency_kit;
  }

  public String getPlates() {
    return plates;
  }

  public void setPlates(String plates) {
    this.plates = plates;
  }

  public boolean getRack() {
    return rack;
  }

  public void setRack(boolean rack) {
    this.rack = rack;
  }

  public boolean getCarpet() {
    return carpet;
  }

  public void setCarpet(boolean carpet) {
    this.carpet = carpet;
  }

  public boolean getHood() {
    return hood;
  }

  public void setHood(boolean hood) {
    this.hood = hood;
  }

    public boolean getTrunk() {
    return trunk;
  }

  public void setTrunk(boolean trunk) {
    this.trunk = trunk;
  }

    public boolean getHubcaps() {
    return hubcaps;
  }

  public void setHubcaps(boolean hubcaps) {
    this.hubcaps = hubcaps;
  }
}
