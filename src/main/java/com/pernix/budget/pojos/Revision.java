package com.pernix.budget.pojos;

public class Revision {

  private String gasLevel;
  private String deliveryPlace;
  private String km;
  private String timestamp;
  private String type;
  private String username;
  private String vehicleMVA;
  private CarParts carParts;
  private String canvas;
  private String licensePlate;

  public String getCanvas() {
    return canvas;
  }

  public void setCanvas(String canvas) {
    this.canvas = canvas;
  }

  public String getGasLevel() {
        return gasLevel;
    }

  public void setGasLevel(String gasLevel) {
        this.gasLevel = gasLevel;
    }

  public String getDeliveryPlace() {
        return deliveryPlace;
    }

  public void setDeliveryPlace(String deliveryPlace) {
        this.deliveryPlace = deliveryPlace;
    }

  public String getKm() {
        return km;
    }

  public void setKm(String km) {
        this.km = km;
    }

  public String getTimestamp() {
        return timestamp;
    }

  public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

  public String getType() {
        return type;
    }

  public void setType(String type) {
        this.type = type;
    }

  public String getUsername() {
        return username;
    }

  public void setUsername(String username) {
        this.username = username;
    }

  public String getVehicleMVA() {
        return vehicleMVA;
    }

  public void setVehicleMVA(String vehicleMVA) {
        this.vehicleMVA = vehicleMVA;
    }

  public CarParts getCarParts() {
    return carParts;
  }

  public void setCarParts(CarParts carParts) {
    this.carParts = carParts;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public void setLicensePlate(String licensePlate) {
    this.licensePlate = licensePlate;
  }

}
