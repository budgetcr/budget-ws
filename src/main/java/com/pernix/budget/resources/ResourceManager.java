package com.pernix.budget.resources;

import javax.annotation.Resource;
import java.io.InputStream;

public class ResourceManager {

    public static InputStream getResourceAsInputStream(String resourceName){
        InputStream inputStreamFile = ResourceManager.class.getResourceAsStream("/" + resourceName);
        return inputStreamFile;
    }
}
